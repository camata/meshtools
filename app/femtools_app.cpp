#include <iostream>
using namespace std;

int main(int argc, char *argv[])
{
   std::cout << "FemTools app: \n";
   std::cout << "  Please see sample code in test/finite_element folder!" << std::endl;
   return 0;
}